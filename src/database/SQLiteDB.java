package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author ryo
 */
public class SQLiteDB {

    private static final String tableName = "fprofile";
    private static final String userColumn = "profileid";
    private static final String print1Column = "fprint";

    private String URL = "jdbc:sqlite:fp.db";
    private String userName;
    private Connection connection = null;
    private String pStmtInsert = "INSERT INTO fprofile (profileid,fprint,status) VALUES (?,?,?);";
    private String pStmtUpdate = "UPDATE fprofile SET fprint=? WHERE profileid=?;";

    public class Record {

        public Long userID;
        public byte[] fmdBinary;

        Record(Long ID, byte[] fmd) {
            userID = ID;
            fmdBinary = fmd;
        }
    }

    public SQLiteDB(String url) {
        URL = url;
    }

    @Override
    public void finalize() {
        try {
            connection.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void Open() throws SQLException {
        connection = DriverManager.getConnection(URL);
    }

    public void Close() throws SQLException {
        connection.close();
    }

    public boolean UserExists(Long userID) throws SQLException {
        String sqlStmt = "Select " + userColumn + " from " + tableName + " WHERE " + userColumn + "='" + userID + "'";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sqlStmt);
        return rs.next();
    }

    public boolean Insert(Long userID, byte[] print1) {
        try {
            PreparedStatement pst = connection.prepareStatement(pStmtInsert);
            pst.setLong(1, userID);
            pst.setBytes(2, print1);
            pst.setInt(3, 1);
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error while registering Fingerprint", "Fingerprint Registration", e.getErrorCode());
        }
        return true;
    }

    public boolean Update(Long userID, byte[] print1) {
        try {
            PreparedStatement pst = connection.prepareStatement(pStmtUpdate);
            pst.setBytes(1, print1);
            pst.setLong(2, userID);
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error while updating Fingerprint", "Fingerprint Registration", e.getErrorCode());
        }
        return true;
    }

    public List<Record> GetAllFPData() throws SQLException {
        List<Record> listUsers = new ArrayList<Record>();

        String sql = "SELECT profileid,fprint FROM fprofile;";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            if (rs.getBytes(print1Column) != null) {
                listUsers.add(new Record(rs.getLong(userColumn), rs.getBytes(print1Column)));
            }
        }
        return listUsers;
    }

    public String GetConnectionString() {
        return URL + " User: " + this.userName;
    }

    public String GetExpectedTableSchema() {
        return "Table: " + tableName + " PK(VARCHAR(32)): " + userColumn + "VARBINARY(4000): " + print1Column;
    }
}
