package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class SQLServerDB {

    private String URL = "jdbc:sqlserver://MYSCTPC004\\SQLEXPRESS2014:1433;databaseName=erp";
    String user = "root";
    String pass = "password";

    private static final String tableName = "fprofile";
    private static final String userColumn = "profileid";
    private static final String print1Column = "fprint";

    private String pStmtInsert = "INSERT INTO fprofile (profileid,fprint,status) VALUES (?,?,?);";
    private String pStmtUpdate = "UPDATE fprofile SET fprint=? WHERE profileid=?;";

    private Connection conn = null;

    public class Record {

        Long userID;
        byte[] fmdBinary;

        Record(Long ID, byte[] fmd) {
            userID = ID;
            fmdBinary = fmd;
        }
    }

    public SQLServerDB(String url) {
        URL = url;
    }

    public void Open() throws SQLException {
        conn = DriverManager.getConnection(URL, user, pass);
    }

    public void Close() throws SQLException {
        conn.close();
    }

    public boolean UserExists(Long userID) throws SQLException {
        String sqlStmt = "Select " + userColumn + " from " + tableName + " WHERE " + userColumn + "='" + userID + "'";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sqlStmt);
        return rs.next();
    }

    public boolean Insert(Long userID, byte[] print1) {
        try {
            PreparedStatement pst = conn.prepareStatement(pStmtInsert);
            pst.setLong(1, userID);
            pst.setBytes(2, print1);
            pst.setInt(3, 1);
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error while registering Fingerprint", "Fingerprint Registration", e.getErrorCode());
        }
        return true;
    }

    public boolean Update(Long userID, byte[] print1) {
        try {
            PreparedStatement pst = conn.prepareStatement(pStmtUpdate);
            pst.setBytes(1, print1);
            pst.setLong(2, userID);
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error while updating Fingerprint", "Fingerprint Registration", e.getErrorCode());
        }
        return true;
    }

    public List<SQLServerDB.Record> GetAllFPData() throws SQLException {
        List<SQLServerDB.Record> listUsers = new ArrayList<SQLServerDB.Record>();

        String sql = "SELECT profileid,fprint FROM fprofile;";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            if (rs.getBytes(print1Column) != null) {
                listUsers.add(new SQLServerDB.Record(rs.getLong(userColumn), rs.getBytes(print1Column)));
            }
        }
        return listUsers;
    }

    public String GetConnectionString() {
        return URL + " User: " + this.user;
    }

    public String GetExpectedTableSchema() {
        return "Table: " + tableName + " PK(VARCHAR(32)): " + userColumn + "VARBINARY(4000): " + print1Column;
    }

}
