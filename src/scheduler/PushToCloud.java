package scheduler;

import crypto.CryptoException;
import crypto.CryptoUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ryo
 */
public class PushToCloud implements Runnable {

    public void run() {
        try {
            PushAttendanceData();
        } catch (SQLException ex) {
            Logger.getLogger(PushToCloud.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void PushAttendanceData() throws SQLException {
        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String key = "Mary has one cat";
            File encryptedFile = new File(dateFormat.format(date) + ".encrypted");
            encryptedFile.createNewFile();

            File file = new File(dateFormat.format(date) + ".txt");
            file.createNewFile();
            CryptoUtils.decrypt(key, encryptedFile, file);
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line = "";
            ArrayList<String> arr = new ArrayList<String>();

            while ((line = br.readLine()) != null) {
                arr.add(line);
            }
            Long[] idArr = new Long[arr.size()];
            for (int i = 0; i < arr.size(); i++) {
                String[] aux = arr.get(i).split(" ");
                idArr[i] = Long.parseLong(aux[0]);
            }
            JSONArray jarr = new JSONArray();
            if (arr.size() > 0) {
                for (int i = 0; i < arr.size(); i++) {

                    String[] aux = arr.get(i).split(" ");
                    Long profileid = Long.parseLong(aux[0]);
                    String[] timeArray = ArrayUtils.remove(aux, 0);
                    JSONObject jobj = new JSONObject();
                    jobj.put("id", profileid);
                    jobj.put("timeArray", timeArray);
                    jarr.add(jobj);
                }
                System.out.println(jarr + " - JSONArray");
            }

            br.close();

//            if (!file.delete()) {
//                System.out.println("Could not delete file");
//                return;
//            }
//
//            CryptoUtils.encrypt(key, file, encryptedFile);
        } catch (IOException ex) {
            Logger.getLogger(scheduler.PushToCloud.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CryptoException ex) {
            Logger.getLogger(scheduler.PushToCloud.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
