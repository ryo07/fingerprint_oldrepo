package crypto;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A tester for the CryptoUtils class.
 *
 * @author www.codejava.net
 *
 */
public class CryptoUtilsTest {

    public static void main(String[] args) {

        try {
//        --  Length should be 16 bytes  --
            String key = "Mary has one cat";
            File inputFile = new File("document.txt");
            inputFile.createNewFile();
            File encryptedFile = new File("document.encrypted");
            File decryptedFile = new File("document.decrypted");

            try {
                CryptoUtils.encrypt(key, inputFile, encryptedFile);
                CryptoUtils.decrypt(key, encryptedFile, decryptedFile);
            } catch (CryptoException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        } catch (IOException ex) {
            Logger.getLogger(CryptoUtilsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
