/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ryo
 */
@Entity
@Table(name = "fprofile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fprofile.findAll", query = "SELECT f FROM Fprofile f"),
    @NamedQuery(name = "Fprofile.findByProfileid", query = "SELECT f FROM Fprofile f WHERE f.profileid = :profileid"),
    @NamedQuery(name = "Fprofile.findByFprint", query = "SELECT f FROM Fprofile f WHERE f.fprint = :fprint"),
    @NamedQuery(name = "Fprofile.findByTimestamp", query = "SELECT f FROM Fprofile f WHERE f.timestamp = :timestamp")})
public class Fprofile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "profileid")
    private Integer profileid;
    @Column(name = "fprint")
    private String fprint;
    @Basic(optional = false)
    @Column(name = "timestamp")
    private String timestamp;

    public Fprofile() {
    }

    public Fprofile(Integer profileid) {
        this.profileid = profileid;
    }

    public Fprofile(Integer profileid, String timestamp) {
        this.profileid = profileid;
        this.timestamp = timestamp;
    }

    public Integer getProfileid() {
        return profileid;
    }

    public void setProfileid(Integer profileid) {
        this.profileid = profileid;
    }

    public String getFprint() {
        return fprint;
    }

    public void setFprint(String fprint) {
        this.fprint = fprint;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profileid != null ? profileid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fprofile)) {
            return false;
        }
        Fprofile other = (Fprofile) object;
        if ((this.profileid == null && other.profileid != null) || (this.profileid != null && !this.profileid.equals(other.profileid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Fprofile[ profileid=" + profileid + " ]";
    }
    
}
