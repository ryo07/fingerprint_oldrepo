/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author ryo
 */
public class TUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;

    private TProfiles tprofile;

    private TSites tsites;

    private AspNetUsers userUuid;

    public TUsers() {
    }

    public TUsers(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TProfiles getTprofile() {
        return tprofile;
    }

    public void setTprofile(TProfiles tprofile) {
        this.tprofile = tprofile;
    }

    public TSites getTsites() {
        return tsites;
    }

    public void setTsites(TSites tsites) {
        this.tsites = tsites;
    }

    public AspNetUsers getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(AspNetUsers userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TUsers)) {
            return false;
        }
        TUsers other = (TUsers) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TUsers[ userId=" + userId + " ]";
    }

}
