/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigInteger;

/**
 *
 * @author ryo
 */
public class TProfiles implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long profileId;

    private String profileSalutation;

    private String profileAvatar;

    private String profileFirstname;

    private String profileLastname;

    private String profileMiddlename;

    private String profileNickname;

    private short profileGender;

    private long profileDob;

    private String profilePhone1;

    private String profilePhone2;

    private String profileOccupation;

    private String profileIncome;

    private String profileMarital;

    private String profileLanguage;

    private String profileRace;

    private String profileReligion;

    private String profileNotes;

    private BigInteger profileNric;

    private Integer profileCreatedat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    private Double profileSalestarget;

    private BigInteger profileMainsite;

    private BigInteger profilePositionid;

    private String profileEmail;

    private String profileSource;

    private String profileMembershipnum;

    private String profileMembershiptype;

    private String profilePassport;

    private String profileCountryoforigin;

    private Integer profileConvertfromlead;

    private BigInteger profilePin;

    private TUsers tusers;

    private TSites tsites;

    public TProfiles() {
    }

    public TProfiles(Long profileId) {
        this.profileId = profileId;
    }

    public TProfiles(Long profileId, String profileSalutation, String profileFirstname, String profileLastname, short profileGender, long profileDob) {
        this.profileId = profileId;
        this.profileSalutation = profileSalutation;
        this.profileFirstname = profileFirstname;
        this.profileLastname = profileLastname;
        this.profileGender = profileGender;
        this.profileDob = profileDob;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public TUsers getTusers() {
        return tusers;
    }

    public void setTusers(TUsers tusers) {
        this.tusers = tusers;
    }

    public TSites getTsites() {
        return tsites;
    }

    public void setTsites(TSites tsites) {
        this.tsites = tsites;
    }

    public String getProfileSalutation() {
        return profileSalutation;
    }

    public void setProfileSalutation(String profileSalutation) {
        this.profileSalutation = profileSalutation;
    }

    public String getProfileAvatar() {
        return profileAvatar;
    }

    public void setProfileAvatar(String profileAvatar) {
        this.profileAvatar = profileAvatar;
    }

    public String getProfileFirstname() {
        return profileFirstname;
    }

    public void setProfileFirstname(String profileFirstname) {
        this.profileFirstname = profileFirstname;
    }

    public String getProfileLastname() {
        return profileLastname;
    }

    public void setProfileLastname(String profileLastname) {
        this.profileLastname = profileLastname;
    }

    public String getProfileMiddlename() {
        return profileMiddlename;
    }

    public void setProfileMiddlename(String profileMiddlename) {
        this.profileMiddlename = profileMiddlename;
    }

    public String getProfileNickname() {
        return profileNickname;
    }

    public void setProfileNickname(String profileNickname) {
        this.profileNickname = profileNickname;
    }

    public short getProfileGender() {
        return profileGender;
    }

    public void setProfileGender(short profileGender) {
        this.profileGender = profileGender;
    }

    public long getProfileDob() {
        return profileDob;
    }

    public void setProfileDob(long profileDob) {
        this.profileDob = profileDob;
    }

    public String getProfilePhone1() {
        return profilePhone1;
    }

    public void setProfilePhone1(String profilePhone1) {
        this.profilePhone1 = profilePhone1;
    }

    public String getProfilePhone2() {
        return profilePhone2;
    }

    public void setProfilePhone2(String profilePhone2) {
        this.profilePhone2 = profilePhone2;
    }

    public String getProfileOccupation() {
        return profileOccupation;
    }

    public void setProfileOccupation(String profileOccupation) {
        this.profileOccupation = profileOccupation;
    }

    public String getProfileIncome() {
        return profileIncome;
    }

    public void setProfileIncome(String profileIncome) {
        this.profileIncome = profileIncome;
    }

    public String getProfileMarital() {
        return profileMarital;
    }

    public void setProfileMarital(String profileMarital) {
        this.profileMarital = profileMarital;
    }

    public String getProfileLanguage() {
        return profileLanguage;
    }

    public void setProfileLanguage(String profileLanguage) {
        this.profileLanguage = profileLanguage;
    }

    public String getProfileRace() {
        return profileRace;
    }

    public void setProfileRace(String profileRace) {
        this.profileRace = profileRace;
    }

    public String getProfileReligion() {
        return profileReligion;
    }

    public void setProfileReligion(String profileReligion) {
        this.profileReligion = profileReligion;
    }

    public String getProfileNotes() {
        return profileNotes;
    }

    public void setProfileNotes(String profileNotes) {
        this.profileNotes = profileNotes;
    }

    public BigInteger getProfileNric() {
        return profileNric;
    }

    public void setProfileNric(BigInteger profileNric) {
        this.profileNric = profileNric;
    }

    public Integer getProfileCreatedat() {
        return profileCreatedat;
    }

    public void setProfileCreatedat(Integer profileCreatedat) {
        this.profileCreatedat = profileCreatedat;
    }

    public Double getProfileSalestarget() {
        return profileSalestarget;
    }

    public void setProfileSalestarget(Double profileSalestarget) {
        this.profileSalestarget = profileSalestarget;
    }

    public BigInteger getProfileMainsite() {
        return profileMainsite;
    }

    public void setProfileMainsite(BigInteger profileMainsite) {
        this.profileMainsite = profileMainsite;
    }

    public BigInteger getProfilePositionid() {
        return profilePositionid;
    }

    public void setProfilePositionid(BigInteger profilePositionid) {
        this.profilePositionid = profilePositionid;
    }

    public String getProfileEmail() {
        return profileEmail;
    }

    public void setProfileEmail(String profileEmail) {
        this.profileEmail = profileEmail;
    }

    public String getProfileSource() {
        return profileSource;
    }

    public void setProfileSource(String profileSource) {
        this.profileSource = profileSource;
    }

    public String getProfileMembershipnum() {
        return profileMembershipnum;
    }

    public void setProfileMembershipnum(String profileMembershipnum) {
        this.profileMembershipnum = profileMembershipnum;
    }

    public String getProfileMembershiptype() {
        return profileMembershiptype;
    }

    public void setProfileMembershiptype(String profileMembershiptype) {
        this.profileMembershiptype = profileMembershiptype;
    }

    public String getProfilePassport() {
        return profilePassport;
    }

    public void setProfilePassport(String profilePassport) {
        this.profilePassport = profilePassport;
    }

    public String getProfileCountryoforigin() {
        return profileCountryoforigin;
    }

    public void setProfileCountryoforigin(String profileCountryoforigin) {
        this.profileCountryoforigin = profileCountryoforigin;
    }

    public Integer getProfileConvertfromlead() {
        return profileConvertfromlead;
    }

    public void setProfileConvertfromlead(Integer profileConvertfromlead) {
        this.profileConvertfromlead = profileConvertfromlead;
    }

    public BigInteger getProfilePin() {
        return profilePin;
    }

    public void setProfilePin(BigInteger profilePin) {
        this.profilePin = profilePin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profileId != null ? profileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TProfiles)) {
            return false;
        }
        TProfiles other = (TProfiles) object;
        if ((this.profileId == null && other.profileId != null) || (this.profileId != null && !this.profileId.equals(other.profileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TProfiles[ profileId=" + profileId + " ]";
    }

}
