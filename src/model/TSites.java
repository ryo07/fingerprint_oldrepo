/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ryo
 */
public class TSites implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long siteId;

    private String siteUuid;

    private Integer siteprintSettings;

    private Integer siteroomQty;

    private String siteroomNames;

    private Integer siteItemfloor;

    private Integer siteItemceiling;

    private Integer siteServicefloor;

    private Integer siteServiceceiling;

    private Integer siteMmsfloor;

    private Integer siteMmsceiling;

    private Integer sitePromofloor;

    private Integer sitePromoceiling;

    private String siteLogo;

    private Collection<TAddresses> tAddressesCollection;

    private TAddresses taddresses;

    private Collection<TProfiles> tProfilesCollection;

    private TProfiles tprofiles;

    private Collection<TUsers> tUsersCollection;

    private TUsers tusers;

    public TSites() {
    }

    public TSites(Long siteId) {
        this.siteId = siteId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSiteUuid() {
        return siteUuid;
    }

    public void setSiteUuid(String siteUuid) {
        this.siteUuid = siteUuid;
    }

    public Integer getSiteprintSettings() {
        return siteprintSettings;
    }

    public void setSiteprintSettings(Integer siteprintSettings) {
        this.siteprintSettings = siteprintSettings;
    }

    public Integer getSiteroomQty() {
        return siteroomQty;
    }

    public void setSiteroomQty(Integer siteroomQty) {
        this.siteroomQty = siteroomQty;
    }

    public String getSiteroomNames() {
        return siteroomNames;
    }

    public void setSiteroomNames(String siteroomNames) {
        this.siteroomNames = siteroomNames;
    }

    public Integer getSiteItemfloor() {
        return siteItemfloor;
    }

    public void setSiteItemfloor(Integer siteItemfloor) {
        this.siteItemfloor = siteItemfloor;
    }

    public Integer getSiteItemceiling() {
        return siteItemceiling;
    }

    public void setSiteItemceiling(Integer siteItemceiling) {
        this.siteItemceiling = siteItemceiling;
    }

    public Integer getSiteServicefloor() {
        return siteServicefloor;
    }

    public void setSiteServicefloor(Integer siteServicefloor) {
        this.siteServicefloor = siteServicefloor;
    }

    public Integer getSiteServiceceiling() {
        return siteServiceceiling;
    }

    public void setSiteServiceceiling(Integer siteServiceceiling) {
        this.siteServiceceiling = siteServiceceiling;
    }

    public Integer getSiteMmsfloor() {
        return siteMmsfloor;
    }

    public void setSiteMmsfloor(Integer siteMmsfloor) {
        this.siteMmsfloor = siteMmsfloor;
    }

    public Integer getSiteMmsceiling() {
        return siteMmsceiling;
    }

    public void setSiteMmsceiling(Integer siteMmsceiling) {
        this.siteMmsceiling = siteMmsceiling;
    }

    public Integer getSitePromofloor() {
        return sitePromofloor;
    }

    public void setSitePromofloor(Integer sitePromofloor) {
        this.sitePromofloor = sitePromofloor;
    }

    public Integer getSitePromoceiling() {
        return sitePromoceiling;
    }

    public void setSitePromoceiling(Integer sitePromoceiling) {
        this.sitePromoceiling = sitePromoceiling;
    }

    public String getSiteLogo() {
        return siteLogo;
    }

    public void setSiteLogo(String siteLogo) {
        this.siteLogo = siteLogo;
    }

    @XmlTransient
    public Collection<TAddresses> getTAddressesCollection() {
        return tAddressesCollection;
    }

    public void setTAddressesCollection(Collection<TAddresses> tAddressesCollection) {
        this.tAddressesCollection = tAddressesCollection;
    }

    @XmlTransient
    public Collection<TProfiles> getTProfilesCollection() {
        return tProfilesCollection;
    }

    public void setTProfilesCollection(Collection<TProfiles> tProfilesCollection) {
        this.tProfilesCollection = tProfilesCollection;
    }

    @XmlTransient
    public Collection<TUsers> getTUsersCollection() {
        return tUsersCollection;
    }

    public void setTUsersCollection(Collection<TUsers> tUsersCollection) {
        this.tUsersCollection = tUsersCollection;
    }

    public TAddresses getTaddresses() {
        return taddresses;
    }

    public void setTaddresses(TAddresses taddresses) {
        this.taddresses = taddresses;
    }

    public TProfiles getTprofiles() {
        return tprofiles;
    }

    public void setTprofiles(TProfiles tprofiles) {
        this.tprofiles = tprofiles;
    }

    public TUsers getTusers() {
        return tusers;
    }

    public void setTusers(TUsers tusers) {
        this.tusers = tusers;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (siteId != null ? siteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TSites)) {
            return false;
        }
        TSites other = (TSites) object;
        if ((this.siteId == null && other.siteId != null) || (this.siteId != null && !this.siteId.equals(other.siteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TSites[ siteId=" + siteId + " ]";
    }

}
