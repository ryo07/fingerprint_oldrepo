
import javax.swing.*;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.digitalpersona.uareu.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ryo
 */
public class Configuration extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1;

    private static final String ACT_SUBMIT = "selection";
    private static final String ACT_EXIT = "exit";

    private JDialog m_dlgParent;
    private JTextArea m_textDomain;

    private ReaderCollection m_collection;
    private Reader m_reader;

    private Configuration() {
        final int vgap = 5;
        final int width = 380;

        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(layout);

        add(Box.createVerticalStrut(vgap));
        JLabel lblReader = new JLabel("Domain Name:");
        add(lblReader);
        add(Box.createVerticalStrut(vgap));
        Dimension dm = lblReader.getPreferredSize();
        dm.width = width;
        lblReader.setPreferredSize(dm);

        m_textDomain = new JTextArea(2, 1);
        m_textDomain.setEditable(true);
        JScrollPane paneReader = new JScrollPane(m_textDomain);
        add(paneReader);
        add(Box.createVerticalStrut(vgap));

        JButton btnCapture = new JButton("Submit");
        btnCapture.setActionCommand(ACT_SUBMIT);
        btnCapture.addActionListener(this);
        btnCapture.setPreferredSize(new Dimension(100, 30));
        add(btnCapture);
        add(Box.createVerticalStrut(vgap));

        add(Box.createVerticalStrut(vgap));
        JButton btnExit = new JButton("Exit");
        btnExit.setActionCommand(ACT_EXIT);
        btnExit.addActionListener(this);
        btnExit.setPreferredSize(new Dimension(100, 30));
        add(btnExit);
        add(Box.createVerticalStrut(vgap));

        setOpaque(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(ACT_SUBMIT)) {
            if (m_textDomain.getText().length() > 0) {
                submit(m_textDomain.getText());
            } else {
                JOptionPane.showMessageDialog(null, "Domain is required!");
            }
        } else if (e.getActionCommand().equals(ACT_EXIT)) {
            m_dlgParent.setVisible(false);
        }
    }

    private void doModal(JDialog dlgParent) {
        m_dlgParent = dlgParent;
        m_dlgParent.setContentPane(this);
        m_dlgParent.pack();
        m_dlgParent.setLocationRelativeTo(null);
        m_dlgParent.setVisible(true);
        m_dlgParent.dispose();
    }

    private void submit(String domain) {

        try {

            File file = new File("config.txt");
            file.createNewFile();

            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            PrintWriter pw = new PrintWriter(bw);
            pw.print(domain);

            pw.close();
            bw.close();

            JOptionPane.showMessageDialog(null, "Domain is successfully registered!");
            m_dlgParent.setVisible(false);
//            EmployeeList.Run();

        } catch (IOException ex) {
            Logger.getLogger(Verification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static boolean getDomain() {

        try {

            String domain = "";
            File file = new File("config.txt");
            file.createNewFile();
            BufferedReader brDomain = new BufferedReader(new FileReader(file));

            if ((domain = brDomain.readLine()) == null) {
                return false;
            }
            domain = brDomain.readLine();
            System.out.println(domain + " - domain");
            brDomain.close();
        } catch (IOException ex) {
            Logger.getLogger(Verification.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    private static void createAndShowGUI() {

//        if (getDomain() == false) {
        Configuration paneContent = new Configuration();

        //initialize capture library by acquiring reader collection
        try {
            paneContent.m_collection = UareUGlobal.GetReaderCollection();
        } catch (UareUException e) {
            MessageBox.DpError("UareUGlobal.getReaderCollection()", e);
            return;
        }

        //run dialog
        JDialog dlg = new JDialog((JDialog) null, "Beautierp Finger Print Application", true);
        paneContent.doModal(dlg);

        //release capture library by destroying reader collection
        try {
            UareUGlobal.DestroyReaderCollection();
        } catch (UareUException e) {
            MessageBox.DpError("UareUGlobal.destroyReaderCollection()", e);
        }
//        } else {
//            EmployeeList.Run();
//        }

    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

}
