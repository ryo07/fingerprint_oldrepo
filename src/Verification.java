
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Engine.Candidate;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import database.SQLiteDB;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import model.TProfiles;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class Verification extends JPanel implements ActionListener {

    private static final long serialVersionUID = 6;
    private static final String ACT_BACK = "back";
    private static final String ACT_CANCEL = "cancel";

    private CaptureThread m_capture;
    private Reader m_reader;
    private Fmd[] m_fmds;
    public Fmd m_enrollmentFmd;
    public SQLiteDB db = new SQLiteDB("jdbc:sqlite:fp.db");
    public List<SQLiteDB.Record> m_listOfRecords = new ArrayList<SQLiteDB.Record>();
    public List<Fmd> m_fmdList = new ArrayList<Fmd>();
    public Fmd[] m_fmdArray = null;

    private JDialog m_dlgParent;
    private JTextArea m_text;
    private final JDialog dlg = new JDialog((JDialog) null, "Staff's Detail", true);

    private final String m_strPrompt1 = "Verification started\n    put any finger on the reader\n\n";
    private final String m_strPrompt2 = "    put the same or any other finger on the reader\n\n";

    private Verification(Reader reader) {
        m_reader = reader;
        m_fmds = new Fmd[2]; //two FMDs to perform comparison

        final int vgap = 5;
        final int width = 380;

        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(layout);

        m_text = new JTextArea(22, 1);
        m_text.setEditable(false);
        JScrollPane paneReader = new JScrollPane(m_text);
        add(paneReader);
        Dimension dm = paneReader.getPreferredSize();
        dm.width = width;
        paneReader.setPreferredSize(dm);

        add(Box.createVerticalStrut(vgap));

        JButton btnBack = new JButton("Back");
        btnBack.setActionCommand(ACT_BACK);
        btnBack.addActionListener(this);
        add(btnBack);
        add(Box.createVerticalStrut(vgap));

        setOpaque(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(ACT_BACK)) {
            //cancel capture
            StopCaptureThread();
        } else if (e.getActionCommand().equals(CaptureThread.ACT_CAPTURE)) {
            //process result
            CaptureThread.CaptureEvent evt = (CaptureThread.CaptureEvent) e;
            if (ProcessCaptureResult(evt)) {
                //restart capture thread
                WaitForCaptureThread();
                StartCaptureThread();
            } else {
                //destroy dialog
                m_dlgParent.setVisible(false);
            }
        } else if (e.getActionCommand().equals(ACT_CANCEL)) {
            dlg.setVisible(false);
        }
    }

    private void StartCaptureThread() {
        m_capture = new CaptureThread(m_reader, false, Fid.Format.ANSI_381_2004, Reader.ImageProcessing.IMG_PROC_DEFAULT);
        m_capture.start(this);
    }

    private void StopCaptureThread() {
        if (null != m_capture) {
            m_capture.cancel();
        }
    }

    private void WaitForCaptureThread() {
        if (null != m_capture) {
            m_capture.join(1000);
        }
    }

    private boolean ProcessCaptureResult(CaptureThread.CaptureEvent evt) {
        boolean bCanceled = false;

        if (null != evt.capture_result) {
            if (null != evt.capture_result.image && Reader.CaptureQuality.GOOD == evt.capture_result.quality) {
                //extract features
                Engine engine = UareUGlobal.GetEngine();

                try {
//                  -- To open connection to SQLite, get the fingerprint byte[] and convert to fmd
                    db.Open();
                    this.m_listOfRecords = db.GetAllFPData();
                    for (SQLiteDB.Record record : this.m_listOfRecords) {
                        Fmd fmd = UareUGlobal.GetImporter().ImportFmd(record.fmdBinary, Fmd.Format.ANSI_378_2004, Fmd.Format.ANSI_378_2004);
                        this.m_fmdList.add(fmd);
                    }
                    m_fmdArray = new Fmd[this.m_fmdList.size()];
                    this.m_fmdList.toArray(m_fmdArray);

                    Fmd fmd = engine.CreateFmd(evt.capture_result.image, Fmd.Format.ANSI_378_2004);
                    if (null == m_fmds[0]) {
                        m_fmds[0] = fmd;
                        int target_falsematch_rate = Engine.PROBABILITY_ONE / 100000; //target rate is 0.00001

                        Candidate[] matches = engine.Identify(m_fmds[0], 0, m_fmdArray, target_falsematch_rate, 1);
                        if (matches.length == 1) {
                            JOptionPane.showMessageDialog(null, "Match found:" + this.m_listOfRecords.get(matches[0].fmd_index).userID);
                            getEmployeeDetail(this.m_listOfRecords.get(matches[0].fmd_index).userID);
                        } else {
                            JOptionPane.showMessageDialog(null, "Not Identified!!!");
                        }
                        m_fmds[0] = null;
                    } else {
                        //the loop continues
                        m_text.append(m_strPrompt2);
                    }
                } catch (UareUException e) {
                    MessageBox.DpError("Engine.CreateFmd()", e);
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    MessageBox.DpError("Failed to load FMDs from database.  Please check connection string in code.", e1);
                }

            } else if (Reader.CaptureQuality.CANCELED == evt.capture_result.quality) {
                //capture or streaming was canceled, just quit
                bCanceled = true;
            } else {
                //bad quality
                MessageBox.BadQuality(evt.capture_result.quality);
            }
        } else if (null != evt.exception) {
            //exception during capture
            MessageBox.DpError("Capture", evt.exception);
            bCanceled = true;
        } else if (null != evt.reader_status) {
            //reader failure
            MessageBox.BadStatus(evt.reader_status);
            bCanceled = true;
        }

        return !bCanceled;
    }

    private void getEmployeeDetail(Long id) {
        try {

            String request = "http://services.aoikumo.com:8080/api/users/user/id/" + id;
            URL url = new URL(request);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Host", "services.aoikumo.com:8080");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer");
            conn.setUseCaches(false);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            StringBuilder content = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                content.append(line);
            }

            Object obj = JSONValue.parse(content.toString());

            JSONObject jobj = new JSONObject();
            jobj = (JSONObject) obj;
            JSONObject profile = (JSONObject) jobj.get("profile");

            TProfiles tprofiles = new TProfiles();
            tprofiles.setProfileIncome((String) profile.get("income"));
            tprofiles.setProfileLastname((String) profile.get("lastName"));
            tprofiles.setProfileMembershiptype((String) profile.get("membershipType"));
            tprofiles.setProfileOccupation((String) profile.get("occupation"));
            tprofiles.setProfileNotes((String) profile.get("notes"));
            tprofiles.setProfileId((Long) jobj.get("iid"));
            tprofiles.setProfilePhone2((String) profile.get("phone2"));
            tprofiles.setProfileLanguage((String) profile.get("language"));
            tprofiles.setProfileSource((String) profile.get("source"));
            tprofiles.setProfilePhone1((String) profile.get("phone1"));
            tprofiles.setProfilePassport((String) profile.get("passport"));
            tprofiles.setProfileEmail((String) profile.get("email"));
            tprofiles.setProfileRace((String) profile.get("race"));
            tprofiles.setProfileNickname((String) profile.get("nickName"));
            tprofiles.setProfileAvatar((String) profile.get("avatar"));
            tprofiles.setProfileReligion((String) profile.get("religion"));
            tprofiles.setProfileMembershipnum((String) profile.get("membershipNumber"));
            tprofiles.setProfileFirstname((String) profile.get("firstName"));
            tprofiles.setProfileMarital((String) profile.get("marital"));
            tprofiles.setProfileMiddlename((String) profile.get("middleName"));
            tprofiles.setProfileSalutation((String) profile.get("salutation"));
            tprofiles.setProfileCountryoforigin((String) profile.get("countryOfOrigin"));

            Long profileid = tprofiles.getProfileId();
            String name = tprofiles.getProfileFirstname() + " " + tprofiles.getProfileLastname();
            String phone1 = tprofiles.getProfilePhone1();
            String phone2 = tprofiles.getProfilePhone2();
            if ("".equals(phone2)) {
                phone2 = "N/A";
            }

            JPanel panel = new JPanel(new GridBagLayout());
            panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Staff's Detail"));

            GridBagConstraints constraints = new GridBagConstraints();
            Border border = BorderFactory.createLineBorder(Color.BLACK);
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(10, 10, 10, 10);

            constraints.gridx = 0;
            constraints.gridy = 0;
            constraints.gridwidth = 2;
            JLabel pidlbl = new JLabel("Profile ID : ");
            panel.add(pidlbl, constraints);

            constraints.gridx = 3;
            constraints.gridy = 0;
            constraints.gridwidth = 2;
            JTextArea pidval = new JTextArea(1, 10);
            pidval.setEditable(false);
            pidval.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 10, 5, 10)));
            pidval.append(profileid.toString());
            panel.add(pidval, constraints);

            constraints.gridx = 0;
            constraints.gridy = 1;
            constraints.gridwidth = 2;
            JLabel namelbl = new JLabel("Name : ");
            panel.add(namelbl, constraints);

            constraints.gridx = 3;
            constraints.gridy = 1;
            constraints.gridwidth = 2;
            JTextArea nameval = new JTextArea(1, 10);
            nameval.setEditable(false);
            nameval.append(name);
            nameval.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 10, 5, 10)));
            panel.add(nameval, constraints);

            constraints.gridx = 0;
            constraints.gridy = 2;
            constraints.gridwidth = 2;
            JLabel phone1lbl = new JLabel("Phone 1 : ");
            panel.add(phone1lbl, constraints);

            constraints.gridx = 3;
            constraints.gridy = 2;
            constraints.gridwidth = 2;
            JTextArea phone1val = new JTextArea(1, 10);
            phone1val.setEditable(false);
            phone1val.append(phone1);
            phone1val.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 10, 5, 10)));
            panel.add(phone1val, constraints);

            constraints.gridx = 0;
            constraints.gridy = 3;
            constraints.gridwidth = 2;
            JLabel phone2lbl = new JLabel("Phone 2 : ");
            panel.add(phone2lbl, constraints);

            constraints.gridx = 3;
            constraints.gridy = 3;
            constraints.gridwidth = 2;
            JTextArea phone2val = new JTextArea(1, 10);
            phone2val.setEditable(false);
            phone2val.append(phone2);
            phone2val.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 10, 5, 10)));
            panel.add(phone2val, constraints);

            JPanel buttonPanel = new JPanel();
            buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
            JButton btnCancel = new JButton("OK");
            btnCancel.setActionCommand(ACT_CANCEL);
            btnCancel.addActionListener(this);
            buttonPanel.add(btnCancel);

            dlg.add(panel, BorderLayout.CENTER);
            dlg.add(buttonPanel, BorderLayout.SOUTH);
            dlg.pack();
            dlg.setAlwaysOnTop(true);
            dlg.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            dlg.setLocationRelativeTo(null);
            dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dlg.setVisible(true);
            conn.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }

    private void doModal(JDialog dlgParent) {
        //open reader
        try {
            m_reader.Open(Reader.Priority.COOPERATIVE);
        } catch (UareUException e) {
            MessageBox.DpError("Reader.Open()", e);
        }

        //start capture thread
        StartCaptureThread();

        //put initial prompt on the screen
        m_text.append(m_strPrompt1);

        //bring up modal dialog
        m_dlgParent = dlgParent;
        m_dlgParent.setContentPane(this);
        m_dlgParent.pack();
        m_dlgParent.setLocationRelativeTo(null);
        m_dlgParent.toFront();
        m_dlgParent.setVisible(true);
        m_dlgParent.dispose();

        //cancel capture
        StopCaptureThread();

        //wait for capture thread to finish
        WaitForCaptureThread();

        //close reader
        try {
            m_reader.Close();
        } catch (UareUException e) {
            MessageBox.DpError("Reader.Close()", e);
        }
    }

    public static void Run(Reader reader) {
        JDialog dlg = new JDialog((JDialog) null, "Verification", true);
        Verification verification = new Verification(reader);
        verification.doModal(dlg);
    }
}
