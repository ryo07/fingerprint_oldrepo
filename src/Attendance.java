
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Engine.Candidate;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import crypto.CryptoException;
import crypto.CryptoUtils;
import database.SQLiteDB;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ryo
 */
public class Attendance extends JPanel implements ActionListener {

    private static final long serialVersionUID = 6;
    private static final String ACT_BACK = "back";
    private static final String ACT_SAVE_TO_DB = "savetodb";

    private CaptureThread m_capture;
    private Reader m_reader;
    private Fmd[] m_fmds;
    public Fmd m_enrollmentFmd;
    public SQLiteDB db = new SQLiteDB("jdbc:sqlite:fp.db");
    public List<SQLiteDB.Record> m_listOfRecords = new ArrayList<SQLiteDB.Record>();
    public List<Fmd> m_fmdList = new ArrayList<Fmd>();
    public Fmd[] m_fmdArray = null;

    private JDialog m_dlgParent;
    private JTextArea m_text;

    private final String m_strPrompt1 = "Attendance Module started\nPlace finger on the reader\n\n";
    private final String m_strPrompt2 = "put the same or any other finger on the reader\n\n";
    private final String m_strPrompt3 = "Stored! Scan Next Please!\n\n";

    private Attendance(Reader reader) {
        m_reader = reader;
        m_fmds = new Fmd[2]; //two FMDs to perform comparison

        final int vgap = 5;
        final int width = 380;

        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(layout);

        m_text = new JTextArea(22, 1);
        m_text.setEditable(false);
        JScrollPane paneReader = new JScrollPane(m_text);
        add(paneReader);
        Dimension dm = paneReader.getPreferredSize();
        dm.width = width;
        paneReader.setPreferredSize(dm);

        add(Box.createVerticalStrut(vgap));

        JButton btnBack = new JButton("Close");
        btnBack.setActionCommand(ACT_BACK);
        btnBack.addActionListener(this);
        add(btnBack);
        add(Box.createVerticalStrut(vgap));
//        -- Button to manually save data to db. Pending
//        JButton btnSavetoDB = new JButton("Save to DB");
//        btnSavetoDB.setActionCommand(ACT_SAVE_TO_DB);
//        btnSavetoDB.addActionListener(this);
//        add(btnSavetoDB);
//        add(Box.createVerticalStrut(vgap));

        setOpaque(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(ACT_BACK)) {
            //cancel capture
            StopCaptureThread();
        } else if (e.getActionCommand().equals(CaptureThread.ACT_CAPTURE)) {
            //process result
            CaptureThread.CaptureEvent evt = (CaptureThread.CaptureEvent) e;
            if (ProcessCaptureResult(evt)) {
                //restart capture thread
                WaitForCaptureThread();
                StartCaptureThread();
            } else {
                //destroy dialog
                m_dlgParent.setVisible(false);
            }
        } else if (e.getActionCommand().equals(ACT_SAVE_TO_DB)) {
            saveDataToDB();
        }
    }

    private void StartCaptureThread() {
        m_capture = new CaptureThread(m_reader, false, Fid.Format.ANSI_381_2004, Reader.ImageProcessing.IMG_PROC_DEFAULT);
        m_capture.start(this);
    }

    private void StopCaptureThread() {
        if (null != m_capture) {
            m_capture.cancel();
        }
    }

    private void WaitForCaptureThread() {
        if (null != m_capture) {
            m_capture.join(1000);
        }
    }

    private boolean ProcessCaptureResult(CaptureThread.CaptureEvent evt) {
        boolean bCanceled = false;

        if (null != evt.capture_result) {
            if (null != evt.capture_result.image && Reader.CaptureQuality.GOOD == evt.capture_result.quality) {
                //extract features
                Engine engine = UareUGlobal.GetEngine();

                try {
                    db.Open();
                    this.m_listOfRecords = db.GetAllFPData();
                    for (SQLiteDB.Record record : this.m_listOfRecords) {
                        Fmd fmd = UareUGlobal.GetImporter().ImportFmd(record.fmdBinary, Fmd.Format.ANSI_378_2004, Fmd.Format.ANSI_378_2004);
                        this.m_fmdList.add(fmd);
                    }
                    m_fmdArray = new Fmd[this.m_fmdList.size()];
                    this.m_fmdList.toArray(m_fmdArray);

                    Fmd fmd = engine.CreateFmd(evt.capture_result.image, Fmd.Format.ANSI_378_2004);
                    if (null == m_fmds[0]) {
                        m_fmds[0] = fmd;
                        int target_falsematch_rate = Engine.PROBABILITY_ONE / 100000; //target rate is 0.00001

                        Candidate[] matches = engine.Identify(m_fmds[0], 0, m_fmdArray, target_falsematch_rate, 1);
                        if (matches.length == 1) {
                            saveDataToFile(this.m_listOfRecords.get(matches[0].fmd_index).userID);
                            m_text.append(m_strPrompt3);
                        } else {
                            JOptionPane.showMessageDialog(null, "Not Identified!!!");
                        }
                        m_fmds[0] = null;
                    } else {
                        //the loop continues
                        m_text.append(m_strPrompt2);
                    }
                } catch (UareUException e) {
                    MessageBox.DpError("Engine.CreateFmd()", e);
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    MessageBox.DpError("Failed to load FMDs from database.  Please check connection string in code.", e1);
                }

            } else if (Reader.CaptureQuality.CANCELED == evt.capture_result.quality) {
                //capture or streaming was canceled, just quit
                bCanceled = true;
            } else {
                //bad quality
                MessageBox.BadQuality(evt.capture_result.quality);
            }
        } else if (null != evt.exception) {
            //exception during capture
            MessageBox.DpError("Capture", evt.exception);
            bCanceled = true;
        } else if (null != evt.reader_status) {
            //reader failure
            MessageBox.BadStatus(evt.reader_status);
            bCanceled = true;
        }

        return !bCanceled;
    }

    private void saveDataToDB() {
        System.out.println("saveDataToDB");
    }

    private void saveDataToFile(Long id) {

        try {

            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            String key = "Mary has one cat";
            File encryptedFile = new File(dateFormat.format(date) + ".encrypted");
            encryptedFile.createNewFile();

            File file = new File(dateFormat.format(date) + ".txt");
            file.createNewFile();
            CryptoUtils.decrypt(key, encryptedFile, file);
            BufferedReader br = new BufferedReader(new FileReader(file));

            File fileWrite = new File(dateFormat.format(date) + ".temp");
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileWrite, true));
            PrintWriter pw = new PrintWriter(bw);

            String line = "";
            ArrayList<String> arr = new ArrayList<String>();
            ArrayList<String> arrWrite = new ArrayList<String>();

            while ((line = br.readLine()) != null) {
                arr.add(line);
            }
            Long[] idArr = new Long[arr.size()];
            for (int i = 0; i < arr.size(); i++) {
                String[] aux = arr.get(i).split(" ");
                idArr[i] = Long.parseLong(aux[0]);
            }

            if (arr.size() > 0) {
                for (int i = 0; i < arr.size(); i++) {
                    BufferedReader brWrite = new BufferedReader(new FileReader(fileWrite));
                    String lineWrite = "";
                    while ((lineWrite = brWrite.readLine()) != null) {
                        arrWrite.add(lineWrite);
                    }

                    Long[] idArr2 = new Long[arrWrite.size()];
                    for (int j = 0; j < arrWrite.size(); j++) {
                        String[] aux = arrWrite.get(j).split(" ");
                        idArr2[j] = Long.parseLong(aux[0]);
                    }

                    String[] aux = arr.get(i).split(" ");
                    Long idFromFile = Long.parseLong(aux[0]);
                    if (idFromFile == id) {
                        String test = arr.get(i).trim();
                        pw.println(test + " " + timeFormat.format(date));
                        pw.flush();
                    } else if (!Arrays.asList(idArr).contains(id) && !Arrays.asList(idArr2).contains(id) && idFromFile != id) {
                        pw.println(id + " " + timeFormat.format(date));
                        pw.println(arr.get(i));
                        pw.flush();
                    } else {
                        pw.println(arr.get(i));
                        pw.flush();
                    }
                    brWrite.close();
                }
            } else {
                pw.println(id + " " + timeFormat.format(date));
                pw.flush();
            }

            pw.close();
            bw.close();
            br.close();
//          -- To delete .txt --
            if (!file.delete()) {
                System.out.println("Could not delete file");
                return;
            }
//          -- To rename .temp to .txt --
            if (!fileWrite.renameTo(file)) {
                System.out.println("Could not rename file");
            }
//          -- To encrypt new .txt file --
            CryptoUtils.encrypt(key, file, encryptedFile);
//          -- To delete the new .txt file --
            if (!file.delete()) {
                System.out.println("Could not delete file");
                return;
            }

        } catch (IOException ex) {
            Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CryptoException ex) {
            Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void doModal(JDialog dlgParent) {
        //open reader
        try {
            m_reader.Open(Reader.Priority.COOPERATIVE);
        } catch (UareUException e) {
            MessageBox.DpError("Reader.Open()", e);
        }

        //start capture thread
        StartCaptureThread();

        //put initial prompt on the screen
        m_text.append(m_strPrompt1);

        //bring up modal dialog
        m_dlgParent = dlgParent;
        m_dlgParent.setContentPane(this);
        m_dlgParent.pack();
        m_dlgParent.setLocationRelativeTo(null);
        m_dlgParent.toFront();
        m_dlgParent.setVisible(true);
        m_dlgParent.dispose();

        //cancel capture
        StopCaptureThread();

        //wait for capture thread to finish
        WaitForCaptureThread();

        //close reader
        try {
            m_reader.Close();
        } catch (UareUException e) {
            MessageBox.DpError("Reader.Close()", e);
        }
    }

    public static void Run(Reader reader, Long oid) {
        JDialog dlg = new JDialog((JDialog) null, "Attendance", true);
        Attendance attendance = new Attendance(reader);
        attendance.doModal(dlg);
    }
}
