
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.simple.JSONObject;

import javax.swing.*;

import com.digitalpersona.uareu.*;
import scheduler.PushToCloud;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import model.TProfiles;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

/**
 *
 * @author ryo
 */
public class Login extends JPanel implements ActionListener {
    
    private static final long serialVersionUID = 1;
    
    Vector outletStrings = getOutletList();
    private final JLabel labelUsername = new JLabel("Enter username:  ");
    private final JLabel labelPassword = new JLabel("Enter password:  ");
    private final JLabel labelOutlet = new JLabel("Select Outlet:  ");
    private final JTextField textUsername = new JTextField(20);
    private final JPasswordField fieldPassword = new JPasswordField(20);
    private final JButton buttonLogin = new JButton("Continue");
    private final JButton btnExit = new JButton("Exit");
    private final JComboBox outletList = new JComboBox(outletStrings);
    
    private static final String LOG_IN = "login";
    private static final String EXIT = "exit";
    
    private JDialog m_dlgParent;
    
    private ReaderCollection m_collection;
    
    private Login() {
        
        JPanel newPanel = new JPanel(new GridBagLayout());
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
//        -- Username & password input --
//        constraints.gridx = 0;
//        constraints.gridy = 0;
//        newPanel.add(labelUsername, constraints);
//
//        constraints.gridx = 1;
//        newPanel.add(textUsername, constraints);
//
//        constraints.gridx = 0;
//        constraints.gridy = 1;
//        newPanel.add(labelPassword, constraints);
//
//        constraints.gridx = 1;
//        newPanel.add(fieldPassword, constraints);
        constraints.gridx = 0;
        constraints.gridy = 3;
        newPanel.add(labelOutlet, constraints);
        
        constraints.gridx = 1;
        outletList.setSelectedIndex(0);
        outletList.addActionListener(this);
        outletList.setPreferredSize(new Dimension(225, 25));
        newPanel.add(outletList, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.EAST;
        buttonLogin.setActionCommand(LOG_IN);
        buttonLogin.addActionListener(this);
        newPanel.add(buttonLogin, constraints);
        
        constraints.gridx = 2;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.WEST;
        btnExit.setActionCommand(EXIT);
        btnExit.addActionListener(this);
        newPanel.add(btnExit, constraints);
        
        newPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Beautierp Login Panel"));
        add(newPanel);
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getActionCommand().equals(LOG_IN)) {
            Item item = (Item) outletList.getSelectedItem();
            m_dlgParent.setVisible(false);
            EmployeeList.Run(item.getId());
        } else if (e.getActionCommand().equals(EXIT)) {
            System.exit(0);
        }
    }
    
    private Vector getOutletList() {
        
        Vector model = new Vector();
        try {
            
            String request = "http://services.aoikumo.com:8080/api/sites/sites";
            URL url = new URL(request);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Host", "services.aoikumo.com:8080");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer");
            conn.setUseCaches(false);
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            
            StringBuilder content = new StringBuilder();
            String line;
            
            while ((line = br.readLine()) != null) {
                content.append(line);
            }
            
            Object obj = JSONValue.parse(content.toString());
            JSONArray finalResult = (JSONArray) obj;
            ArrayList<TProfiles> tparr = new ArrayList<TProfiles>();
            
            if (finalResult != null) {
                for (int i = 0; i < finalResult.size(); i++) {
                    JSONObject jobj = new JSONObject();
                    jobj = (JSONObject) finalResult.get(i);
                    JSONObject profile = (JSONObject) jobj.get("profile");
                    TProfiles tprofiles = new TProfiles();
                    
                    tprofiles.setProfileIncome((String) profile.get("income"));
                    tprofiles.setProfileLastname((String) profile.get("lastName"));
                    tprofiles.setProfileMembershiptype((String) profile.get("membershipType"));
                    tprofiles.setProfileOccupation((String) profile.get("occupation"));
                    tprofiles.setProfileNotes((String) profile.get("notes"));
                    tprofiles.setProfileId((Long) profile.get("iid"));
                    tprofiles.setProfilePhone2((String) profile.get("phone2"));
                    tprofiles.setProfileLanguage((String) profile.get("language"));
                    tprofiles.setProfileSource((String) profile.get("source"));
                    tprofiles.setProfilePhone1((String) profile.get("phone1"));
                    tprofiles.setProfilePassport((String) profile.get("passport"));
                    tprofiles.setProfileEmail((String) profile.get("email"));
                    tprofiles.setProfileRace((String) profile.get("race"));
                    tprofiles.setProfileNickname((String) profile.get("nickName"));
                    tprofiles.setProfileAvatar((String) profile.get("avatar"));
                    tprofiles.setProfileReligion((String) profile.get("religion"));
                    tprofiles.setProfileMembershipnum((String) profile.get("membershipNumber"));
                    tprofiles.setProfileFirstname((String) profile.get("firstName"));
                    tprofiles.setProfileMarital((String) profile.get("marital"));
                    tprofiles.setProfileMiddlename((String) profile.get("middleName"));
                    tprofiles.setProfileSalutation((String) profile.get("salutation"));
                    tprofiles.setProfileCountryoforigin((String) profile.get("countryOfOrigin"));
                    tparr.add(tprofiles);
                }
            }
            
            for (int i = 0; i < tparr.size(); i++) {
                model.addElement(new Item(tparr.get(i).getProfileId(), tparr.get(i).getProfileFirstname() + " " + tparr.get(i).getProfileLastname()));
            }
            
            conn.disconnect();
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
        return model;
    }
    
    class Item {
        
        private Long id;
        private String outlet;
        
        public Item(Long id, String outlet) {
            this.id = id;
            this.outlet = outlet;
        }
        
        public Long getId() {
            return id;
        }
        
        public String getOutlet() {
            return outlet;
        }
        
        public String toString() {
            return outlet;
        }
    }
    
    private void doModal(JDialog dlgParent) {
        m_dlgParent = dlgParent;
        m_dlgParent.setContentPane(this);
        m_dlgParent.pack();
        m_dlgParent.setLocationRelativeTo(null);
        m_dlgParent.setVisible(true);
        m_dlgParent.dispose();
    }
    
    public static void createAndShowGUI() {
        Login paneContent = new Login();
        
        try {
            paneContent.m_collection = UareUGlobal.GetReaderCollection();
        } catch (UareUException e) {
            MessageBox.DpError("UareUGlobal.getReaderCollection()", e);
            return;
        }
        
        JDialog dlg = new JDialog((JDialog) null, "Beautierp Finger Print Application", true);
        paneContent.doModal(dlg);
        
        try {
            UareUGlobal.DestroyReaderCollection();
        } catch (UareUException e) {
            MessageBox.DpError("UareUGlobal.destroyReaderCollection()", e);
        }
    }
    
    public static void Run() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

//  -- To execute PushToCloud.java under scheduler package
    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
    Thread st = new Thread(new PushToCloud());
    ScheduledFuture scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(st, 5, 10, TimeUnit.SECONDS);
    
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
        
    }
    
}
