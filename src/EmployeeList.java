
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.json.simple.JSONObject;

import com.digitalpersona.uareu.*;
import com.digitalpersona.uareu.Reader;
import java.util.ArrayList;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import model.TProfiles;

/**
 *
 * @author ryo
 */
public class EmployeeList extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1;

    private final JButton buttonExit = new JButton("Exit");
    private final JButton buttonSearch = new JButton("FingerPrint Enrollment");
    private final JButton buttonSelect = new JButton("Select Reader");
    private final JButton buttonGet = new JButton("View Staff Detail");
    private final JButton buttonRet = new JButton("Return");
    private final JButton buttonAttend = new JButton("Attendance");
    JDialog dlg = new JDialog((JDialog) null, "Outlet's Staff List", true);

    GridBagConstraints gbc = new GridBagConstraints();

    static final int width = 1000;
    static final int hight = 600;
    static final int vgap = 5;

    private static final String EXIT = "exit";
    private static final String ACT_SEARCH = "search";
    private static final String ACT_SELECTION = "selection";
    private static final String ACT_GET = "getprofile";
    private static final String ACT_RET = "return";
    private static final String ACT_CANCEL = "cancel";
    private static final String ACT_ATTEND = "attendence";

    private JDialog m_dlgParent;
    private JTextArea m_textReader;
    public static Long oid;

    private static ReaderCollection m_collection;
    private static Reader m_reader;

    private EmployeeList() {

        add(Box.createVerticalStrut(vgap));
        JLabel lblReader = new JLabel("Selected reader:");
        add(lblReader);
        add(Box.createVerticalStrut(vgap));
        Dimension dm = lblReader.getPreferredSize();
        dm.width = 100;
        lblReader.setPreferredSize(dm);

        m_textReader = new JTextArea(2, 40);
        m_textReader.setEditable(false);
        JScrollPane paneReader = new JScrollPane(m_textReader);
        add(paneReader);
        add(Box.createVerticalStrut(vgap));

        JPanel newPanel = new JPanel(new GridBagLayout());
        newPanel.setSize(width, hight);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);

        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        buttonAttend.setActionCommand(ACT_ATTEND);
        buttonAttend.addActionListener(this);
        newPanel.add(buttonAttend, constraints);

        constraints.gridx = 3;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        buttonSearch.setActionCommand(ACT_SEARCH);
        buttonSearch.addActionListener(this);
        newPanel.add(buttonSearch, constraints);

//        -- Button to select reader --
//        constraints.gridx = 6;
//        constraints.gridy = 4;
//        constraints.gridwidth = 2;
//        buttonSelect.setActionCommand(ACT_SELECTION);
//        buttonSelect.addActionListener(this);
//        newPanel.add(buttonSelect, constraints);
        constraints.gridx = 6;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        buttonGet.setActionCommand(ACT_GET);
        buttonGet.addActionListener(this);
        newPanel.add(buttonGet, constraints);

        constraints.gridx = 0;
        constraints.gridy = 5;
        constraints.gridwidth = 2;
        buttonRet.setActionCommand(ACT_RET);
        buttonRet.addActionListener(this);
        newPanel.add(buttonRet, constraints);

        constraints.gridx = 3;
        constraints.gridy = 5;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        buttonExit.setActionCommand(EXIT);
        buttonExit.addActionListener(this);
        newPanel.add(buttonExit, constraints);

        newPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Sub Menu"));
        add(newPanel);

        setReader();

    }

    public void setReader() {

        try {
            this.m_collection = UareUGlobal.GetReaderCollection();
            m_collection.GetReaders();
        } catch (UareUException e1) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error getting collection");
            return;
        }

        if (m_collection.size() == 0) {
            MessageBox.Warning("Reader is not selected");
            return;
        }

        m_reader = m_collection.get(0);
//        System.out.println("m_reader is " + m_reader.GetDescription().name);
        if (null != m_reader) {
            m_textReader.setText(m_reader.GetDescription().name);
        } else {
            m_textReader.setText("");
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals(ACT_SELECTION)) {
//          -- Redirect to Selection.java where all available Readers will be listed
//            m_reader = Selection.Select(m_collection);
            if (null != m_reader) {
                m_textReader.setText(m_reader.GetDescription().name);
            } else {
                m_textReader.setText("");
            }
        } else if (e.getActionCommand().equals(ACT_SEARCH)) {
            if (null == m_reader) {
                MessageBox.Warning("Reader is not selected");
            } else {
                searchEmployee();
            }
        } else if (e.getActionCommand().equals(EXIT)) {
            m_dlgParent.setVisible(false);
        } else if (e.getActionCommand().equals(ACT_GET)) {
            if (null == m_reader) {
                MessageBox.Warning("Reader is not selected");
            } else {
                Verification.Run(m_reader);
            }
        } else if (e.getActionCommand().equals(ACT_RET)) {
            m_dlgParent.setVisible(false);
            Login.Run();
        } else if (e.getActionCommand().equals(ACT_CANCEL)) {
            dlg.setVisible(false);
        } else if (e.getActionCommand().equals(ACT_ATTEND)) {
            if (null == m_reader) {
                MessageBox.Warning("Reader is not selected");
            } else {
                Attendance.Run(m_reader, oid);
            }
        }
    }

    private void searchEmployee() {
        try {
//            -- TO LOAD DOMAIN FROM FILE --
//            String domain = "";
//            File file = new File("config.txt");
//            try (BufferedReader brDomain = new BufferedReader(new FileReader(file))) {
//                domain = brDomain.readLine();
//            }
//            String request = domain + ":8080/api/users/attendance/users";

            String request = "http://services.aoikumo.com:8080/api/users/getusersbysite/" + this.oid;
            URL url = new URL(request);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Host", "services.aoikumo.com:8080");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer");
            conn.setUseCaches(false);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            StringBuilder content = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                content.append(line);
            }

            Object obj = JSONValue.parse(content.toString());
            JSONArray finalResult = (JSONArray) obj;

            ArrayList<TProfiles> tparray = new ArrayList<TProfiles>();

            if (finalResult != null) {
                for (int i = 0; i < finalResult.size(); i++) {
                    JSONObject jobj = new JSONObject();
                    jobj = (JSONObject) finalResult.get(i);

                    JSONObject profile = (JSONObject) jobj.get("profile");
                    TProfiles tprofiles = new TProfiles();
                    tprofiles.setProfileIncome((String) profile.get("income"));
                    tprofiles.setProfileLastname((String) profile.get("lastName"));
                    tprofiles.setProfileMembershiptype((String) profile.get("membershipType"));
                    tprofiles.setProfileOccupation((String) profile.get("occupation"));
                    tprofiles.setProfileNotes((String) profile.get("notes"));
//                    tprofiles.setProfileId((Long) profile.get("iid"));
                    tprofiles.setProfileId((Long) jobj.get("iid"));
                    tprofiles.setProfilePhone2((String) profile.get("phone2"));
                    tprofiles.setProfileLanguage((String) profile.get("language"));
                    tprofiles.setProfileSource((String) profile.get("source"));
                    tprofiles.setProfilePhone1((String) profile.get("phone1"));
                    tprofiles.setProfilePassport((String) profile.get("passport"));
                    tprofiles.setProfileEmail((String) profile.get("email"));
                    tprofiles.setProfileRace((String) profile.get("race"));
                    tprofiles.setProfileNickname((String) profile.get("nickName"));
                    tprofiles.setProfileAvatar((String) profile.get("avatar"));
                    tprofiles.setProfileReligion((String) profile.get("religion"));
                    tprofiles.setProfileMembershipnum((String) profile.get("membershipNumber"));
                    tprofiles.setProfileFirstname((String) profile.get("firstName"));
                    tprofiles.setProfileMarital((String) profile.get("marital"));
                    tprofiles.setProfileMiddlename((String) profile.get("middleName"));
                    tprofiles.setProfileSalutation((String) profile.get("salutation"));
                    tprofiles.setProfileCountryoforigin((String) profile.get("countryOfOrigin"));
                    tparray.add(tprofiles);
                }
            }

            String col[] = {"ID", "Name", "Phone 1", "Phone 2", "Status"};
            DefaultTableModel tableModel = new DefaultTableModel(col, 0);

            JTable table = new JTable(tableModel);
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
            table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            table.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
//          -- To make Table clickable and to get value --
            table.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 1) {
                        JTable target = (JTable) e.getSource();
                        int row = target.getSelectedRow();
                        Long value = (Long) target.getValueAt(row, 0);
                        dlg.setVisible(false);
                        Enrollment.Run(m_reader, value);
                    }
                }
            });

            for (int i = 0; i < tparray.size(); i++) {
                Long profileid = tparray.get(i).getProfileId();
                String name = tparray.get(i).getProfileFirstname() + " " + tparray.get(i).getProfileLastname();
                String phone1 = tparray.get(i).getProfilePhone1();
                String phone2 = tparray.get(i).getProfilePhone2();

                Object[] data = {profileid, name, phone1, phone2, "N/A"};

                tableModel.addRow(data);
            }

            JScrollPane sp = new JScrollPane(table);

            JPanel btns = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
            JButton btnCancel = new JButton("Cancel");
            btnCancel.setActionCommand(ACT_CANCEL);
            btnCancel.addActionListener(this);
            btnCancel.setPreferredSize(new Dimension(100, 30));
            btns.add(btnCancel);

            dlg.add(sp, BorderLayout.CENTER);
            dlg.add(btns, BorderLayout.SOUTH);
            dlg.pack();
            dlg.setAlwaysOnTop(true);
            dlg.setBackground(Color.BLUE);
            dlg.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            dlg.setLocationRelativeTo(null);
            dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dlg.setVisible(true);

            conn.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }

    private void doModal(JDialog dlgParent) {
        m_dlgParent = dlgParent;
        m_dlgParent.setContentPane(this);
        m_dlgParent.pack();
        m_dlgParent.setLocationRelativeTo(null);
        m_dlgParent.setVisible(true);
        m_dlgParent.dispose();
    }

    public static void createAndShowGUI() {
        EmployeeList paneContent = new EmployeeList();

        try {
            paneContent.m_collection = UareUGlobal.GetReaderCollection();
        } catch (UareUException e) {
            MessageBox.DpError("UareUGlobal.getReaderCollection()", e);
            return;
        }

        JDialog dlg = new JDialog((JDialog) null, "Beautierp Finger Print Application", true);
        paneContent.doModal(dlg);

        try {
            UareUGlobal.DestroyReaderCollection();
        } catch (UareUException e) {
            MessageBox.DpError("UareUGlobal.destroyReaderCollection()", e);
        }
    }

    public static void Run(Long outletid) {
        oid = outletid;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

//    public static void main(String[] args) {
//        javax.swing.SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                createAndShowGUI();
//            }
//        });
//
//    }
}
